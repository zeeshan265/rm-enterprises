<?php

namespace App\Http\Controllers\ProductManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Toastr;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product.index')->with('products',Product::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = new Product();
        return view('product.create')->with('product',$products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            "name" => "string|required",
            "batch_no" => "string|required|unique:products",
            'mfg_price' => 'integer|required|',
            "sale_price" => 'integer|required|',
            "mfg_date" => 'required|date_format:d/m/y',
            "exp_date" => 'required|date_format:d/m/y'


        ]);

        $product = new Product;
        $product->name = $request->name;
        $product->batch_no = $request->batch_no;
        $product->mfg_price = $request->mfg_price;
        $product->sale_price = $request->sale_price;
        $product->mfg_date = Carbon::createFromFormat('d/m/y', $request->mfg_date)->toDateString();
        $product->exp_date = Carbon::createFromFormat('d/m/y', $request->exp_date)->toDateString();
        $product->slug = Str::slug($request->name,'-');
        $product->user_id = Auth::id();




        $product->save();
        Toastr::success('Prpduct added Successfully', 'Success', ["positionClass" => "toast-bottom-right"]);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug){

        $product = Product::where('slug', $slug)->firstorFail();
        return view("product.show", compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $product =  Product::where('slug',$slug)->firstOrFail();
        return view('product.create')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $this->validate($request,[
            "name" => "string|required",
            "batch_no" => "string|required",
            'mfg_price' => 'integer|required|',
            "sale_price" => 'integer|required|',
            "mfg_date" => 'required|date_format:d/m/y',
            "exp_date" => 'required|date_format:d/m/y'
        ]);
        $product = Product::where('slug',$slug)->first();

        $product->name = $request->name;
        $product->batch_no = $request->batch_no;
        $product->mfg_price = $request->mfg_price;
        $product->sale_price = $request->sale_price;
        $product->mfg_date = Carbon::createFromFormat('d/m/y', $request->mfg_date)->toDateString();
        $product->exp_date = Carbon::createFromFormat('d/m/y', $request->exp_date)->toDateString();
        $product->slug = Str::slug($request->name,'-');
        $product->user_id = Auth::id();

         $product->save();
        Toastr::success('Product updated Successfully', 'Success', ["positionClass" => "toast-bottom-right"]);

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $product =  Product::where('slug',$slug)->firstOrFail();
        $product->delete();
        Toastr::success('Product Deleted Successfully', 'Success', ["positionClass" => "toast-bottom-right"]);
        return redirect()->route('products.index');
    }
}

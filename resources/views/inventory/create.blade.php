@extends('layouts.app')


@section('title', 'Inventory')
@section('titleicon', 'fa fa-product-hunt')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('inventory.partials.form', ['inventory', $inventory])
        </div>
    </div>
@endsection

@extends('layouts.app')


@section('title', $inventory->name)
@section('titleicon', 'fa fa-product-hunt')

@section('content')
    <div class="row tab-content">
        <div class="col-sm-4 col-lg-3" id="info">
            @include('inventory.partials.menu')
        </div>
        <div class="col-sm-8 col-lg-9 tab-pane active" id="users">
            @include('inventory.partials.info', ['inventory', $product])
        </div>
    </div>
@endsection

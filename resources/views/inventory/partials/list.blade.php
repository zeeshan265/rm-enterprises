@extends('layouts.block' , [ 'b_type' => 'table', "b_options" => 'ft'])

@section('b-title')
    <h2> <i class="fa fa-product-hunt"></i> <strong> Inventory </strong></h2>
@overwrite
@section('b-subtitle')
    List
@overwrite

@section('b-options')
    <a href="{{ route('inventorys.create') }}" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="" data-original-title="Create Product"><i class="fa fa-plus"></i></a>
@overwrite

@section('b-thead')
    <tr>
        <th>ID</th>
        <th>Product Name</th>
        <th>Stock</th>
        <th>Expiry Date</th>
        <th class="text-center">Actions</th>
    </tr>
@endsection

@section('b-tbody')
    @foreach ($inventorys as $row)
        <tr>
            <td class="text-center">
                {{ $row->id }}
            </td >
            <td class="text-center">
                {{ $row->product->name }}
            </td >
            <td class="text-center">
                {{ $row->stock }}
            </td>
            <td class="text-center">
                {{ $row->product->exp_date->format('d-M-Y') }}
            </td>
            
            
            <td class="text-center">
                <div class="btn-group">

                    <a href="{{ route('inventorys.edit',$row->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                    

                </div>
                <form method="post" id="delete-form">
       @method('DELETE')
       @csrf
   </form>
            </td>
        </tr>
    @endforeach
    

@endsection

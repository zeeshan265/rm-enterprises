@extends('layouts.block')

@section('b-title')
    <h2> <i class="fa fa-product-hunt"></i> <strong> Add Inventory  </strong> </h2>
@endsection


@section('b-content')
    <form method="POST" class="form-horizontal form-bordered" action="{{route('inventorys.store') }}" autocomplete="off">
        
       
        @csrf
       

        <div class="form-group @error('name') has-error @enderror @if(session('danger')) has-error @endif">
            <label class="col-md-3 control-label" for="name">Product Name <span class="text-danger">*</span></label>
            <div class="col-md-9">
                <select id="example-chosen" name="product_id" class="select-chosen" data-placeholder="Choose a Product.." style="width: 250px;">
                        <option></option>
                        @foreach( $products as $product)
                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                </select>
                @error('name')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
                @if(session('danger'))
                    <span class="help-block animation-slideDown text-danger">{{session('danger')}}</span>
                @endif
            </div>
        </div>
        
        
        <div class="form-group @error('stock') has-error @enderror">
            <label class="col-md-3 control-label" for="stock">Stock <span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="stock" name="stock" class="form-control" value="{{ old('stock', $inventory->stock) }}"       placeholder="Stock In Packets" required >
                @error('stock')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
       <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane"></i> Submit</button>
                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            </div>
        </div>
    </form>
@endsection

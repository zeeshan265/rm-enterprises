@extends('layouts.app')


@section('title', 'Inventory Information')
@section('titleicon', 'fa fa-product-hunt')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('inventory.partials.list', ['inventorys', $inventorys,'products',$products])
        </div>
    </div>
@endsection

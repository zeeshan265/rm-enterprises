@extends('layouts.app')


@section('title', 'Product Information')
@section('titleicon', 'fa fa-product-hunt')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('product.partials.list', ['products', $products])
        </div>
    </div>
@endsection

@extends('layouts.block', ["b_options" => 'ft'])

@section('b-title')
   <h2> <i class="fa fa-file-o"></i> <strong>{{ $product->name }}</strong> Info</h2>
@overwrite

@section('b-content')
    @parent
    <table class="table table-borderless table-striped">
        <tbody>
        <tr>
            <td style="width: 20%;"><strong>Batch No</strong></td>
            <td>{{ $product->batch_no  }}</td>
        </tr>
        <tr>
            <td><strong>Manufactured Price</strong></td>
            <td> {{ $product->mfg_price}} </td>
        </tr>
        <tr>
            <td><strong>Sale Price</strong></td>
            <td> {{ $product->sale_price ?? "Null" }} </td>
        </tr>
         <tr>
            <td><strong>Manufactured Date</strong></td>
            <td> {{ $product->mfg_date ?? "Null" }} </td>
        </tr>
        <tr>
            <td><strong>Expiry Date</strong></td>
            <td> {{ $product->exp_date ?? "Null" }} </td>
        </tr>
        <tr>
            <td><strong>Registered Date</strong></td>
            <td> {{ $product->created_at  }} </td>
        </tr>
        <tr>
            <td><strong>Last Update</strong></td>
            <td> {{ $product->updated_at  }} </td>
        </tr>
        
        </tbody>
    </table>

@overwrite

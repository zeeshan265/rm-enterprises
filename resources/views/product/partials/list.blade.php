@extends('layouts.block' , [ 'b_type' => 'table', "b_options" => 'ft'])

@section('b-title')
    <h2> <i class="fa fa-product-hunt"></i> <strong> Products </strong></h2>
@overwrite
@section('b-subtitle')
    List
@overwrite

@section('b-options')
    <a href="{{ route('products.create') }}" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="" data-original-title="Create Product"><i class="fa fa-plus"></i></a>
@overwrite

@section('b-thead')
    <tr>
        <th class="text-center">ID</th>
        <th>Title</th>
        <th>Batch No</th>
        <th class="text-center">Sale Price</th>
        <th class="text-center">Expiry Date</th>
        <th class="text-center">Option</th>
        <th class="text-center">Actions</th>
    </tr>
@endsection

@section('b-tbody')
    @foreach ($products as $row)
        <tr>
            <td class="text-center">
                {{ $row->id }}
            </td>
            <td class="text-center">
                {{ $row->name }}
            </td >
            <td class="text-center">
                {{ $row->batch_no }}
            </td>
            <td class="text-center">
                {{ $row->sale_price }}
            </td>
            <td class="text-center">
                {{ $row->exp_date->format('d-M-Y') }}
            </td>
            <td class="text-center">
                
                <a href=" {{ route('carts.add_to_cart',$row->id) }}" class="btn btn-alt btn-xs btn-danger">Add to Cart</a>
               
                
            </td>
            <td class="text-center">
                <div class="btn-group">
                    <a href="{{ route('products.show', $row->slug) }}" data-toggle="tooltip" title="View" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>

                    <a href="{{ route('products.edit',$row->slug) }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                    <a href="{{ route('products.destroy',$row->slug) }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger confirm-delete"><i class="fa fa-times"></i></a>
                    

                </div>
                <form method="post" id="delete-form">
       @method('DELETE')
       @csrf
   </form>
            </td>
        </tr>
    @endforeach
    

@endsection

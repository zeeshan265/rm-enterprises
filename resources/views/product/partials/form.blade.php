@extends('layouts.block')

@section('b-title')
    <h2> <i class="fa fa-product-hunt"></i> <strong> Add Product  </strong> </h2>
@endsection


@section('b-content')
    <form method="POST" class="form-horizontal form-bordered" action="{{ ($product->slug) ? route('products.update', $product->slug ) : route('products.store') }}" autocomplete="off">
        
       
        @csrf
        @if($product->slug)
            @method('PUT')
        @else
            @method('POST')
        @endif

        <div class="form-group @error('name') has-error @enderror @if(session('danger')) has-error @endif">
            <label class="col-md-3 control-label" for="name">Name <span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $product->name) }}"placeholder=" Product Name" required autofocus>
                @error('name')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
                @if(session('danger'))
                    <span class="help-block animation-slideDown text-danger">{{session('danger')}}</span>
                @endif
            </div>
        </div>
        <div class="form-group @error('batch_no') has-error @enderror">
            <label class="col-md-3 control-label" for="batch_no">Batch No <span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="batch_no" name="batch_no" class="form-control" value="{{ old('batch_no', $product->batch_no) }}"       placeholder="Product Batch No" required >
                @error('batch_no')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group @error('mfg_price') has-error @enderror">
            <label class="col-md-3 control-label" for="mfg_price">Manufactured Price<span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="mfg_price" name="mfg_price" class="form-control" value="{{ old('mfg_price', $product->mfg_price) }}"       placeholder="Product Batch No" required >
                @error('mfg_price')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group @error('sale_price') has-error @enderror">
            <label class="col-md-3 control-label" for="sale_price">Sale Price<span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="sale_price" name="sale_price" class="form-control" value="{{ old('sale_price', $product->sale_price) }}"       placeholder="Product Batch No" required >
                @error('sale_price')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group @error('mfg_date') has-error @enderror">
            <label class="col-md-3 control-label" for="mfg_date">Manufactured Date <span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="example-datepicker5" name="mfg_date" class="form-control input-datepicker-close" {{-- value=" {{old('mfg_date', $product->mfg_date->format('d/m/Y'))}}"  --}}data-date-format="dd/mm/yy" placeholder="dd/mm/yy">
                @error('mfg_date')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
                @enderror
            </div>

        </div>
        <div class="form-group @error('exp_date') has-error @enderror">
            <label class="col-md-3 control-label" for="exp_date">Expiry<span class="text-danger">*</span></label>
            <div class="col-md-9">
                <input type="text" id="example-datepicker5" name="exp_date" class="form-control input-datepicker-close"{{--  value=" {{old('exp_date', $product->exp_date->format('d/m/Y'))}}"  --}}data-date-format="dd/mm/yy" placeholder="dd/mm/yy">
                @error('exp_date')
                <span class="help-block animation-slideDown text-danger">{{ $message }}</span>
              
                @enderror
            </div>

        </div>


        

        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-paper-plane"></i> Submit</button>
                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            </div>
        </div>
    </form>
@endsection

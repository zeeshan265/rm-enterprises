@extends('layouts.app')


@section('title', 'Product')
@section('titleicon', 'fa fa-product-hunt')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('product.partials.form', ['product', $product])
        </div>
    </div>
@endsection
